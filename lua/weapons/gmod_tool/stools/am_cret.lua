TOOL.Category = "Anti-Matter Reactors"
TOOL.Name = "#AM Creator"
TOOL.Command = nil
TOOL.ConfigName = ""
if (CLIENT and GetConVarNumber("CAF_UseTab") == 1) then TOOL.Tab = "Custom Addon Framework" end

TOOL.ClientConVar["type"] = "am_cret"
TOOL.ClientConVar["model"] = "models/partaccel/partaccel.mdl"

if ( CLIENT ) then
	language.Add( "tool.am_cret.name", "Anti-Matter Generators" )
	language.Add( "tool.am_cret.desc", "Spawns A generator for use with Anti-Matter Reactors." )
	language.Add( "tool.am_cret.0", "Left Click: Spawn A generator. Right Click: Repair A Storage" )
	
	language.Add( "Undone_am_cret", "Anti-Matter Creator Undone" )
	language.Add( "Cleanup_am_cret", "Anti-Matter Creator" )
	language.Add( "Cleaned_am_cret", "Cleaned up all Anti-Matter Creator" )
	language.Add( "SBoxLimit_am_cret", "Maximum Anti-Matter Storages Creator" )
end

if not CAF or not CAF.GetAddon("Resource Distribution") then Error("Please Install Resource Distribution Addon.'" ) return end
if not CAF or not CAF.GetAddon("Life Support") then return end

if( SERVER ) then
    CreateConVar("sbox_maxam_cret", 2)
    
    function Makeam_cret( ply, ang, pos, gentype, model, frozen )
        if ( !ply:CheckLimit( "am_creator" ) ) then return nil end
        
        local ent = ents.Create( gentype )
        
        ent:SetPos( pos )
        ent:SetAngles( ang )
        
        ent:Spawn()
        ent:Activate()
        
        ent:SetVar("Owner", ply)
        ent:SetPlayer(ply)
        
        ent.Class = gentype
        
        if(frozen) then
            local phys = ent:GetPhysicsObject()
            if(phys:IsValid()) then
                phys:EnableMotion( false )
                ply:AddFrozenPhysicsObject(ent, phys)
                end
            end
        
        ply:AddCount("am_cret", ent)
        
        return ent
    end
    
    duplicator.RegisterEntityClass("Small Anti-Matter Creator", Makeam_cret, "Ang", "Pos", "Class", "model", "frozen")
end
local am_cret_models = {
		{ "#Small Anti-Matter Creator", "models/partaccel/partaccel.mdl", "am_scret" }
}
CAF_ToolRegister( TOOL,am_cret_models,Makeam_creator,"am_cret",2) 
TOOL.Category = "Anti-Matter Reactors"
TOOL.Name = "#AM Storage"
TOOL.Command = nil
TOOL.ConfigName = ""
if (CLIENT and GetConVarNumber("CAF_UseTab") == 1) then TOOL.Tab = "Custom Addon Framework" end

TOOL.ClientConVar["type"] = "am_storage"
TOOL.ClientConVar["model"] = "models/ce_ls3additional/resource_cache/resource_cache_large.mdl"

if ( CLIENT ) then
	language.Add( "tool.am_storage.name", "Anti-Matter Storages" )
	language.Add( "tool.am_storage.desc", "Spawns A Storage for use with Anti-Matter Reactors." )
	language.Add( "tool.am_storage.0", "Left Click: Spawn A Storage. Right Click: Repair A Storage" )
	
	language.Add( "Undone_am_storage", "Anti-Matter Storage Undone" )
	language.Add( "Cleanup_am_storage", "Anti-Matter Storage" )
	language.Add( "Cleaned_am_storage", "Cleaned up all Anti-Matter Storages" )
	language.Add( "SBoxLimit_am_storage", "Maximum Anti-Matter Storages Reached" )
end

if not CAF or not CAF.GetAddon("Resource Distribution") then Error("Please Install Resource Distribution Addon.'" ) return end
if not CAF or not CAF.GetAddon("Life Support") then return end

if( SERVER ) then
    CreateConVar("sbox_maxam_storage", 2)
    
    function Makeam_storage( ply, ang, pos, stortype, model, frozen )
        if ( !ply:CheckLimit( "am_storage" ) ) then return nil end
        
        local ent = ents.Create( stortype )
        
        ent:SetPos( pos )
        ent:SetAngles( ang )
        
        ent:Spawn()
        ent:Activate()
        
        ent:SetVar("Owner", ply)
        ent:SetPlayer(ply)
        
        ent.Class = stortype
        
        if(frozen) then
            local phys = ent:GetPhysicsObject()
            if(phys:IsValid()) then
                phys:EnableMotion( false )
                ply:AddFrozenPhysicsObject(ent, phys)
                end
            end
        
        ply:AddCount("am_storage", ent)
        
        return ent
    end
    
    duplicator.RegisterEntityClass("Small Anti-Matter Storage", Makeam_storage, "Ang", "Pos", "Class", "model", "frozen")
    duplicator.RegisterEntityClass("Medium Anti-Matter Storage", Makeam_storage, "Ang", "Pos", "Class", "models", "frozen")
    duplicator.RegisterEntityClass("Large Anti-Matter Storage", Makeam_storage, "Ang", "Pos", "Class", "models", "frozen")
end
local am_stor_models = {
		{ "#Small Anti-Matter Storage", "models/smallstor/smallstor.mdl", "am_sstore" },
        { "#Medium Anti-Matter Storage", "models/mandrac/resource_cache/huge_cache.mdl", "am_mstore"},
        { "#Large Anti-Matter Storage", "models/mandrac/resource_cache/colossal_cache.mdl", "am_lstore" }
}
CAF_ToolRegister( TOOL,am_stor_models,Makeam_storage,"am_storage",2) 
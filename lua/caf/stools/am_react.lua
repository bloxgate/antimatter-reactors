TOOL.Category = "Anti-Matter Reactors"
TOOL.Name = "#AM Reactors"

TOOL.DeviceName = "Anti-Matter Reactor"
TOOL.DeviceNamePlural = "Anti-Matter Reactors"
TOOL.ClassName = "am_reactor"

TOOL.DevSelect = true
TOOL.CCVar_type = "am_react"
TOOL.CCVar_sub_type = "default_am"
TOOL.CCVar_model = "models/ce_ls3additional/fusion_generator/fusion_generator_small.mdl"

TOOL.Limited = true
TOOL.LimitName = "am_reactor"
TOOL.Limit = 1

CAFToolSetup.SetLang("Anti-Matter Reactors", "Attach an AM reactor to a surface", "Left-Click to spawn a device.")
local RD = CAF.GetAddon("Resource Distribution")

function TOOL.EnableFunc()
	local SB = CAF.GetAddon("Spacebuild")
	local RD = CAF.GetAddon("Resource Distribution")
	if not SB or not SB.GetStatus() or not RD or not RD.GetStatus() then
		return false;
	end
	return true;
end

TOOL.Renamed = {
    class = {
        am_reactor = "anti_matter_reactor"
    },
    type = {
        am_reactor = "anti_matter_reactor"
    },
}
local function anti_matter_reactor_func(ent,type,sub_type,devinfo)
    local mass = 50000
    ent.mass = mass
    local maxhealth = 2500
    return mass, maxhealth
end

TOOL.Devices = {
    anti_matter_reactor = {
        Name = "Small Anti-Matter Reactors",
        type = "anti_matter_reactor",
        class = "am_sreact",
        func = anti_matter_reactor_func,
        devices = {
            am_sreact = {
                Name = "Small Cylindrical Reactor",
                model = "models/ce_ls3additional/fusion_generator/fusion_generator_small.mdl",
                skin = 0,
                legacy = false,
            },
            am_spreact = {
                Name = "Small PosiSphere Reactor",
                model = "models/posisphere/posisphere.mdl",
                skin = 0,
                legacy = false,
            },
        },
    },
}
   

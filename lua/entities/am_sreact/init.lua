AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
util.PrecacheSound( "Airboat_engine_idle" )
util.PrecacheSound( "Airboat_engine_stop" )

include('shared.lua')

function ENT:Initialize()
	self.BaseClass.Initialize(self)

	local phys = self.Entity:GetPhysicsObject()
	self.damaged = 0
	self.Active = 0
	
	self:SetMaxHealth(2500)
	self:SetHealth(self:GetMaxHealth())
	
	self.multiply = 1
	
	-- resource attributes
	self.amcon = 1 --AM production
	self.eprod = 1000 -- Energy consumption
    
	CAF.GetAddon("Resource Distribution").AddResource(self,"Anti-Matter",0)
	if WireLib then
		self.WireDebugName = self.PrintName
		self.Inputs = WireLib.CreateInputs(self, { "On", "Mute"})
		self.Outputs = WireLib.CreateOutputs(self, { "On", "Energy Production", "AM Consumtion", "Health"})
	end
	
	if (phys:IsValid()) then
		phys:Wake()
		phys:SetMass(50000)
	end
end

function ENT:Setup()
	self:TriggerInput("On", 0)
	self:TriggerInput("Overdrive", 0)
	self:TriggerInput("Mute", 0)
end

function ENT:TriggerInput(iname, value)
	if (iname == "On") then
		if (value > 0) then
			if ( self.Active == 0 ) then
				self:TurnOn()
		else
			if ( self.Active == 1 ) then
				self:TurnOff()
			end
		end
	elseif (iname == "Mute") then
		if (value >= 1) then
			if (self.mute == 0) then
				self.mute = 1
				self.Entity:StopSound( "Airboat_engine_idle" )
				self.Entity:StopSound( "Airboat_engine_stop" )
			end
		else
			if( self.mute == 1) then
				self.mute = 0
				if(self.Active == 1) then
					self.Entity:EmitSound( "Airboat_engine_idle" )
				end
			end
		end
    end
end
end

function ENT:OnRemove()
	self.BaseClass.OnRemove(self)
	self.Entity:StopSound( "Airboat_engine_idle" )
	self.Entity:StopSound( "common/warning.wav" )
	self.Entity:StopSound( "Airboat_engine_stop" )
end

function ENT:Damage()
	if (self.damaged == 0) then
		self.damaged = 1
	end
	if ((self.Active == 1) and self:Health() <= 20) then
		self.eprod = 2000
	end
end

function ENT:Repair()
	self.Entity:SetColor(255, 255, 255, 255)
	self:SetHealth(self:GetMaxHealth())
	self.damaged = 0
    self.eprod = 1000
end

function ENT:TurnOn()
	self.Active = 1
	self:SetOOO(1)
	if WireLib then 
		WireLib.TriggerOutput(self, "On", 1)
	end
	if(self.mute == 0) then
		self.Entity:EmitSound( "Airboat_engine_idle" )
	end
end

function ENT:TurnOff()
	self.Active = 0
	self.overdrive = 0
	self:SetOOO(0)
	if WireLib then
		WireLib.TriggerOutput(self, "On", 0)
	end
	
	self.Entity:StopSound( "Airboat_engine_idle" )
	if(self.mute == 0) then
		self.Entity:EmitSound( "Airboat_engine_stop" )
	end
end

function ENT:Destruct()
    CAF.GetAddon("Life Support").Destruct( self.Entity )
end

function ENT:Output()
	return 1
end

function ENT:MakeEnergy() 
	if ( self:CanRun() ) then
		self:SupplyResource("energy", self.eprod)
		self:ConsumeResource("Anti-Matter",self.amcon)
        if CAF and CAF.GetAddon("Life Support") then
            CAF.GetAddon("Life Support").DamageLS(self, math.random(10,50))
        else
            self:SetHealth( self:Health( ) - math.random(10,50))
            if self:Health() <= 0 then
                self:Remove()
            end
        end
		if not (WireAddon == nil) then Wire_TriggerOutput(self.Entity, "On", 1) end
	else
		self:TurnOff()
	end
	
	if WireLib then
      WireLib.TriggerOutput(self, "AM Consumption", self.amcon)
      WireLib.TriggerOutput(self, "Energy Production", self.eprod)
      WireLib.TriggerOutput(self, "Health", self:Health())
	end
		
	return
end

function ENT:CanRun()
	local am = self:GetResourceAmount("Anti-Matter")
	if (am > 0) then
        return true
	else
		return false
	end
end

function ENT:Think()
    self.BaseClass.Think(self)

	if ( self.Active == 1 ) then
		self:MakeEnergy()
	end
    
	self.Entity:NextThink( CurTime() + 1 )
	return true
end

function ENT:AcceptInput(name,activator,caller)
	if name == "Use" and caller:IsPlayer() and caller:KeyDownLast(IN_USE) == false then
		if ( self.Active == 0 ) then
			self:TurnOn()
		elseif (self.Active == 1) then
			self:TurnOff()
		end
	end
end

function ENT:PreEntityCopy()
    self.BaseClass.PreEntityCopy(self)
end

function ENT:PostEntityPaste( Player, Ent, CreatedEntities )
    self.BaseClass.PostEntityPaste(self, Player, Ent, CreatedEntities )
end

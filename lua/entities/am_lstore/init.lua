AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include('shared.lua')

function ENT:Initialize()
    self.Entity:SetModel( "models/mandrac/resource_cache/colossal_cache.mdl")
    self.BaseClass.Initialize(self)
    
    local phys = self.Entity:GetPhysicsObject()
    if(phys:IsValid()) then
        phys:Wake()
        phys:SetMass(100000)
    end
    
    self.damaged = 0
    self:SetMaxHealth(200000)
    self:SetHealth(self:GetMaxHealth())
    
    CAF.GetAddon("Resource Distribution").AddResource(self,"Anti-Matter",2000000000)
    
    if WireLib then
        self.WireDebugName = self.PrintName
        self.Outputs = WireLib.CreateOutputs(self,{"AM","Max AM"})
    end
end

function ENT:OnRemove()
    self.BaseClass.OnRemove(self)
end

function ENT:Damage()
    if(self.damaged == 0) then
        self.damaged = 1
    end
end

function ENT:Repair()
    self.Entity:SetColor(255,255,255,255)
    self:SetHealth(self:GetMaxHealth())
    self.damaged = 0
end

function ENT:Destruct()
    CAF.GetAddon("Life Support").Destruct( self.Entity )
end

function ENT:Output()
    return 1
end

function ENT:UpdateWireOutputs()
    if WireLib then
        WireLib.TriggerOutput(self, "AM", self:GetResourceAmount("Anti-Matter"))
        WireLib.TriggerOutput(self, "Max AM", self:GetNetworkCapacity("Anti-Matter"))
    end
end

function ENT:Think()
    self.BaseClass.Think(self)
    
    self:UpdateWireOutputs()
    
	self.Entity:NextThink( CurTime() + 1 )
	return true
end

function ENT:AcceptInput(name,activator,caller)
	if name == "Use" and caller:IsPlayer() and caller:KeyDownLast(IN_USE) == false then
		local amcur = self:GetResourceAmount( "Anti-Matter" )
		caller:ChatPrint("There is "..tostring(amcur).." Anti-Matter stored in this resource network.")
	end
end

function ENT:PreEntityCopy()
    self.BaseClass.PreEntityCopy(self)
end

function ENT:PostEntityPaste( Player, Ent, CreatedEntities )
    self.BaseClass.PostEntityPaste(self, Player, Ent, CreatedEntities )
end
